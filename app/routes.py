import os
from threading import Thread

from flask import render_template, request, jsonify
from werkzeug.utils import secure_filename
from app import app, pin
from app.services import database, quote, google_article_search, utils
from app.services.google_article_search import search
from config import Config


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')


@app.route('/search', methods=["GET"])
def pin_search():
    query = request.args.get(key='query', type=str)
    board_id = request.args.get(key='board_id', type=str)
    re_pin_count = request.args.get(key='rePinCount', type=int)
    thread = Thread(target=pin.search, args=(query, re_pin_count, board_id))
    thread.daemon = True
    thread.start()

    return "Pinterest search has been started!"


@app.route('/google_article_search', methods=["GET"])
def google_article_search():
    query = request.args.get(key='query', type=str)
    count = request.args.get(key='count', type=int)
    board_id = request.args.get(key='board_id', type=str)
    description_prefix = request.args.get(key='description_prefix', type=str)
    google_descr_length = request.args.get(key='google_descr_length', type=int)
    search(query, count, board_id, description_prefix, google_descr_length)
    # thread = Thread(target=google_article_search.search, args=(query, count, pictures_count, board_id, description_prefix, google_descr_length))
    # thread.daemon = True
    # thread.start()
    return "Google article search has started!"


@app.route('/re_pin', methods=["GET"])
def re_pin():
    query = request.args.get(key='query', type=str)
    count = request.args.get(key='count', type=int)
    board_id = request.args.get(key='board_id', type=str)
    thread = Thread(target=pin.re_pins, args=(count, board_id, query))
    thread.daemon = True
    thread.start()
    return "Pinterest rePin has been started!"


@app.route("/search_terms", methods=["GET"])
def search_terms():
    return jsonify(database.counts_pins())


@app.route("/board_ids", methods=["GET"])
def board_ids():
    username = request.args.get(key='username', type=str)
    return jsonify(pin.list_board_ids(username))


@app.route("/local_fonts", methods=["GET"])
def local_fonts():
    return {'List of fonts': quote.read_fonts()}


@app.route("/schedule_repin", methods=["GET"])
def schedule_re_pins():
    # amount of pins means amount of pins to post taken from each category for each day
    amount = request.args.get(key='amount', type=int)
    days_amount = request.args.get(key='daysAmount', type=int)
    repin_count = request.args.get(key='repin_count', type=int)
    search_terms_to_include = request.args.get(key='search_terms_to_include', type=str).strip()
    search_terms_to_exclude = request.args.get(key='search_terms_to_exclude', type=str).strip()
    thread = Thread(target=pin.schedule_re_pins,
                    args=(days_amount, amount, repin_count, search_terms_to_include, search_terms_to_exclude))
    thread.daemon = True
    thread.start()
    return "Pinterest SCHEDULE Re Pins has been started!"


@app.route("/schedule_set", methods=["GET"])
def schedule_set():
    # amount of pins means amount of pins to post taken from each category for each day
    days_amount = request.args.get(key='daysAmount', type=int)
    repin_amount = request.args.get(key='Repin amount for 1 day', type=int)
    repin_count = request.args.get(key='repin_count', type=int)
    google_articles_count = request.args.get(key='Google articles amount for 1 day', type=int)
    google_images_count = request.args.get(key='Google images amount for 1 day', type=int)
    quotes_count = request.args.get(key='Quotes amount for 1 day', type=int)
    my_pins_count = request.args.get(key='My pins amount for 1 day', type=int)
    search_terms_to_include = request.args.get(key='search_terms_to_include', type=str).strip()
    search_terms_to_exclude = request.args.get(key='search_terms_to_exclude', type=str).strip()
    time_ranges = request.args.get(key='Time ranges for scheduler', type=str).strip()
    thread = Thread(target=pin.schedule_set,
                    args=(days_amount,
                          repin_amount, repin_count,
                          google_articles_count,
                          google_images_count,
                          quotes_count,
                          my_pins_count,
                          search_terms_to_include, search_terms_to_exclude,
                          time_ranges))
    thread.daemon = True
    thread.start()
    return "Pinterest SCHEDULE SET has been started!"
    """return pin.schedule_set (days_amount,
                            repin_amount, repin_count,
                            google_articles_count,
                            google_images_count,
                            quotes_count,
                            my_pins_count,
                            search_terms_to_include, search_terms_to_exclude,
                            time_ranges)"""


@app.route('/follow', methods=["GET"])
def pin_follow():
    count = request.args.get(key='count', type=int)
    days_amount = request.args.get(key='daysAmount', type=int)
    username_followers = request.args.get(key='usernameFollowers', type=str)
    time_ranges = request.args.get(key='Time ranges for follow', type=str).strip()
    thread = Thread(target=pin.follow, args=(count, username_followers, days_amount, time_ranges))
    thread.daemon = True
    thread.start()

    return "Pinterest follow has been started!"


@app.route("/upload_pins", methods=["GET"])
def upload_pins():
    return jsonify(pin.upload_pins())


@app.route("/set_boards", methods=["GET"])
def set_boards():
    return jsonify(pin.set_boards())


@app.route("/get_boards", methods=["GET"])
def get_boards():
    return jsonify(pin.get_boards())


@app.route('/uploads', methods=['POST'])
def uploads():
    # Before using this route please define that are you would like doing.
    # param key ['logo'] -> uploads logo
    # param key ['fonts'] -> uploads fonts
    # param key ['quotes'] -> uploads quotes
    # param key ['my_pins'] -> uploads my pins

    # check if the post request has the file part
    keys = ['quotes', 'logo', 'fonts', 'my_pins']
    key = ''

    for e in keys:
        if e in request.files:
            key += e

    if key is '':
        return f'You use no valid download key. Please use this: {keys}'

    file = request.files[key]
    # if user does not select file, browser also
    # submit an empty part without filename
    if file.filename == '':
        return 'No selected file. Please choose file!'
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        path = os.path.join(define_folder_path(key), filename)
        file.save(path)

        if key is 'quotes':
            quote.draw_quotes(path)

        if key is 'my_pins':
            print("my_pins")
            utils.zip_extract(path, filename)

        return f'File | {filename} | has been uploaded!'
    else:
        return f'You can only download these files in these extensions: {Config.ALLOWED_FILE_EXTENSIONS}'


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in Config.ALLOWED_FILE_EXTENSIONS


def define_folder_path(key):
    if key == 'quotes':
        return Config.UPLOAD_TMP_FOLDER
    elif key == 'logo':
        return Config.UPLOAD_LOGO_FOLDER
    elif key == 'fonts':
        return Config.UPLOAD_FONTS_FOLDER
    elif key == 'my_pins':
        return Config.UPLOAD_TMP_FOLDER
    else:
        return Config.UPLOAD_FOLDER
