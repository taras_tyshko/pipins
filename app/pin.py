import csv
import json
import os
from os import listdir
import random
import shutil
from config import Config
from pathlib import Path
from random import randint
from app.models import Pin, Follow, Board
from config import Config
from .services import database
from .services.pinterest import pinterest
from datetime import datetime, timedelta
from apscheduler.schedulers.blocking import BlockingScheduler
import numpy as np
import shutil

# TIME_RANGES =[[3, 6], [9, 11], [14, 18], [21, 23], [20, 22]]
TIME_RANGES_FILENAME = 'time_ranges_pin.csv'
TIME_RANGES = []
TIME_RANGES_FULL = {}

TIME_RANGES_FOLLOW_FILENAME = 'time_ranges_follow.csv'
TIME_RANGES_FOLLOW = []
TIME_RANGES_FOLLOW_FULL = {}

PINS_AMOUNT_MAP = {}
FOLLOWS_AMOUNT_MAP = {}
IDS_TO_FOLLOW = {}
ALL_PINS = []

SCHEDULE = BlockingScheduler(job_defaults={'misfire_grace_time': 15 * 60})

SEARCH_TERMS_TO_INCLUDE_ARR = []
SEARCH_TERMS_TO_EXCLUDE_ARR = []


def search(query, repin_count, board_id):
    """
    Implementation of Pinterest search
    :param query:
    :param repin_count:
    :param board_id:
    """
    # After change in pinterest API, you can no longer search for users
    # Instead you need to search for something else and extract the user data from there.
    # current pinterest scopes are: pins, buyable_pins, my_pins, videos, boards
    results = []
    ids = []
    pins_in_db = []
    # Getting pin "ID" from database
    # Getting of Pinterest pins
    for item in Pin().get_pin_ids():
        pins_in_db.append(item.pin_id)

    search_batch = pinterest.search(scope='pins', query=query)
    while len(search_batch) > 0 and len(ids) < 500:
        for pin in search_batch:
            # Checking type pin
            if pin["type"] == "pin":
                # Getting and add pin "ID"
                ids.append(pin["id"])

        # Getting next pins
        search_batch = pinterest.search(scope='pins', query=query)
    # Filter and set unique pin "ID" to results variable
    results += difference(ids, pins_in_db)
    get_pin_id(results, query, repin_count, board_id)


def list_board_ids(username):
    output_boards = []

    boards = pinterest.boards(username=username)
    for board in boards:
        output_boards.append({'name': board['name'], 'id': board['id']})
    return output_boards


def follow(count, username_followers, days_amount, time_ranges):
    # time_ranges_reader()
    process_time_ranges(time_ranges, TIME_RANGES_FOLLOW)

    users_result = get_followers(username_followers, count, 5)
    i = 0

    print('Ids to follow', IDS_TO_FOLLOW)
    if count > len(IDS_TO_FOLLOW):
        print('There are not enough users to follow - ,', count, ', available users amount - ', len(IDS_TO_FOLLOW))
        count = len(IDS_TO_FOLLOW)
    if len(IDS_TO_FOLLOW) > 0:
        # def scheduleActivity(amount, targetMap, time_to_schedule, time_to_schedule_full, method, arguments,
        # daysAmount):
        schedule_activity(count, FOLLOWS_AMOUNT_MAP, TIME_RANGES_FOLLOW, TIME_RANGES_FOLLOW_FULL, follow_single,
                          IDS_TO_FOLLOW, days_amount)


def get_followers(username, max_items, max_batch_amount):
    print('MAX ITEMS=', max_items)
    followers = []
    new_followers = []
    followers_batch = pinterest.get_user_followers(username=username)
    batch_counter = 0
    i = 0

    while len(followers_batch) > 0 and len(IDS_TO_FOLLOW) < max_items and batch_counter < max_batch_amount:
        # print('followers_batch', followers_batch)
        new_followers = followers_batch
        for follower in new_followers:
            # check if we already follow the user
            if not database.check_if_follow(follower['id']):
                IDS_TO_FOLLOW[i] = [follower['id']]
                i += 1
                # print('follower=', follower)
        # followers += followers_batch
        followers += new_followers
        followers_batch = pinterest.get_user_followers(username=username)
        batch_counter += 1

    return followers


def follow_single(user_id=''):
    # even if you already follow this user a successful message is returned
    request = pinterest.follow_user(user_id=user_id)
    if request.status_code == 200:
        Follow(user_id=user_id).update(dict(we_follow=1))
    else:
        pass
    return


def get_pin_id(pins, search_term, re_pin_count, board_id):
    """
    Getting pin from Pinterest and check if param "repin_count" > re_pin_count. And save in database.
    :param pins:
    :param search_term:
    :param re_pin_count:
    :param board_id:
    """
    for pin in pins:
        try:
            # Getting data from Pinterest
            new_pin = pinterest.load_pin(pin_id=pin)
            # Check whether the pin has the right amount
            if new_pin['pins'][f'{pin}']['repin_count'] >= re_pin_count:
                print(Pin(pin_id=f'{new_pin["pins"][pin]["id"]}', search_term=search_term, board_id=board_id).insert())
            else:
                pass
        except:
            pass


def re_pins(count, board_id, search_term):
    """
    Function implementation rePin on Pinterest.
    :param count:
    :param board_id:
    :param search_term:
    """
    # Getting pins form database
    pins = Pin.query.filter_by(search_term=search_term, status=0).limit(count).all()
    print(pins)
    for pin in pins:
        # Do repin on the Pinterest
        request = pinterest.repin(board_id=board_id, pin_id=pin.pin_id)
        # Check if request is OK
        if request.status_code == 200:
            # Update status in database for current pin
            print(Pin(pin_id=pin.pin_id).update(dict(status=1)))
        else:
            pass


def re_pin_single(pin_id, board_id):
    # Do repin on the Pinterest
    request = pinterest.repin(board_id=board_id, pin_id=pin_id)
    # Check if request is OK
    if request.status_code == 200:
        # Update status in database for current pin
        print(Pin(pin_id=pin_id).update(dict(status=1)))
    else:
        pass


def schedule_set(days_amount,
                 repin_amount, repin_count,
                 google_articles_count,
                 google_images_count,
                 quotes_count,
                 my_pins_count,
                 search_terms_to_include, search_terms_to_exclude,
                 time_ranges):
    process_time_ranges(time_ranges, TIME_RANGES)
    print('TIME_RANGES=', TIME_RANGES)

    # checking if we have enough pins of each type
    enough_all_pins = True
    pin_types_needed = []

    if search_terms_to_include != '' and search_terms_to_exclude != '':
        print(search_terms_to_include)
        print(search_terms_to_exclude)
        print("You can use only one parameter among search terms to include OR search terms to exclude values")
        return "You can use only one parameter among search terms to include OR search terms to exclude values"

    SEARCH_TERMS_TO_INCLUDE_ARR = search_terms_to_include.split(',')
    SEARCH_TERMS_TO_EXCLUDE_ARR = search_terms_to_exclude.split(',')

    types_to_check = {'google_article': {'folder_name': Config.GOOGLE_ARTICLES_PINS_FOLDER,
                                         'pins_per_day': google_articles_count},
                      'quote': {'folder_name': Config.UPLOAD_QUOTES_FOLDER,
                                'pins_per_day': quotes_count},
                      'my_pin': {'folder_name': Config.UPLOAD_MY_PINS_FOLDER,
                                 'pins_per_day': my_pins_count},
                      'google_image': {'folder_name': Config.GOOGLE_IMAGES_PINS_FOLDER,
                                       'pins_per_day': google_images_count}
                      }
    pins_in_db_include = []
    pins_in_db_exclude = []
    id_to_board_id = []

    if len(SEARCH_TERMS_TO_INCLUDE_ARR) != 0:
        print('search_terms_to_include')
        print(SEARCH_TERMS_TO_INCLUDE_ARR)
        pins_in_db_include = Pin.query.filter(Pin.search_term.in_(SEARCH_TERMS_TO_INCLUDE_ARR)).with_entities(
            Pin.pin_id, Pin.board_id).all()
        ids_to_include = [el[0] for el in pins_in_db_include]
        id_to_board_id = {el[0]: el[1] for el in pins_in_db_include}
        print('id_to_board_id')
        print(id_to_board_id)
        print('ids_to_include', ids_to_include)
    elif len(SEARCH_TERMS_TO_EXCLUDE_ARR) != 0:
        print('search_terms_to_exclude')
        print(SEARCH_TERMS_TO_EXCLUDE_ARR)
        pins_in_db_exclude = Pin.query.filter(Pin.search_term.in_(SEARCH_TERMS_TO_EXCLUDE_ARR)).with_entities(
            Pin.pin_id).all()
        ids_to_exclude = [el[0] for el in pins_in_db_exclude]

    for key in types_to_check:
        value = types_to_check[key]

        ids = listdir(value['folder_name'])
        ids_we_can_use = set(ids)
        if (SEARCH_TERMS_TO_INCLUDE_ARR != '') and key in ('google_article', 'google_image'):
            ids_we_can_use = set(ids).intersection(set(ids_to_include))
            print('ids_we_can_use=', ids_we_can_use)
        elif (SEARCH_TERMS_TO_EXCLUDE_ARR != '') and key in ('google_article', 'google_image'):
            ids_we_can_use = set(ids).difference(set(ids_to_exclude))
            print('ids_we_can_use=', ids_we_can_use)
        value['is_enough'] = True if len(ids_we_can_use) >= value['pins_per_day'] * days_amount else False
        value['ids_we_can_use'] = ids_we_can_use
        enough_all_pins = enough_all_pins and value['is_enough']
        if not value['is_enough']:
            pin_types_needed.append(key)

    now = datetime.now()
    days = []
    day_folder_names = []
    if enough_all_pins:
        for day in range(days_amount):
            start_date_time = now + timedelta(days=day)
            day_date = start_date_time.date()
            days.append(day_date)
            print("Creating a folder with pins for the day ", day, " - ", day_date)
            day_folder_name = os.path.join(Config.SCHEDULE_SET_FOLDER, str(day_date))
            day_folder_names.append(day_folder_name)

            if not os.path.exists(day_folder_name):
                os.makedirs(day_folder_name)
        # move pins to he folders separated by days
        # pins_to_schedule = move_pins(days_amount, day_folder_names, types_to_check)
        # then schedule pins from day_folders + schedule repins
        if repin_amount > 0:
            schedule_re_pins(days_amount, repin_amount, repin_count)

    else:
        str_to_return = "**************************************************\n" \
                        "There are not enough items of the following types:\n"
        for el in pin_types_needed:
            str_to_return += el + '\n'
        str_to_return += "**************************************************\n"
        print(str_to_return)
        return str_to_return


def move_pins(days_amount, day_folder_names, types_to_check):
    move_folders = {}

    for key in types_to_check:
        type_el = types_to_check[key]
        if type_el['pins_per_day'] > 0:
            # copy type_el['pins_per_day'] pins into day_folder_names folders
            ids_to_use = list(type_el['ids_we_can_use'])
            pins_needed = type_el['pins_per_day'] * days_amount
            pins_per_day = type_el['pins_per_day']
            """for el in ids_to_use:
                print(el)"""
            day_counter = 0
            for day in day_folder_names:
                move_folders[day] = []
                for i in range(day_counter * pins_per_day, day_counter * pins_per_day + pins_per_day):
                    move_folders[day].append(os.path.join(type_el['folder_name'], ids_to_use[i]))
                    # move_ids[day].append(ids_to_use[i])
                day_counter += 1
    print('move_folders', move_folders)
    # moving the folders
    for key in move_folders:
        el_to_move = move_folders[key]
        for folder in el_to_move:
            shutil.move(folder, key)
    return move_folders


def process_time_ranges(time_ranges, target_time_ranges):
    ranges = time_ranges.split(',')
    for t_range in ranges:
        hours = t_range.split('-')
        target_time_ranges.append([int(hours[0]), int(hours[1])])


def schedule_re_pins(days_amount, amount, repin_count):
    global ALL_PINS

    search_terms = database.get_search_terms()
    print(search_terms)
    now = datetime.now()

    if len(SEARCH_TERMS_TO_INCLUDE_ARR) > 0:
        for search_term in SEARCH_TERMS_TO_INCLUDE_ARR:
            print(search_term)
            get_pins_by_single_search_term(search_term, amount * days_amount, repin_count)
    elif len(SEARCH_TERMS_TO_EXCLUDE_ARR) > 0:
        for search_term in search_terms:
            if not (search_term in SEARCH_TERMS_TO_EXCLUDE_ARR):
                print(search_term)
                get_pins_by_single_search_term(search_term, amount * days_amount, repin_count)
    else:
        for search_term in search_terms:
            print(search_term)
            get_pins_by_single_search_term(search_term, amount * days_amount, repin_count)

    schedule_activity(len(ALL_PINS), PINS_AMOUNT_MAP, TIME_RANGES, TIME_RANGES_FULL, re_pin_single, ALL_PINS,
                      days_amount)


def get_pins_by_single_search_term(search_term, count, repin_count):
    global ALL_PINS

    pins = database.get_pins_by_terms(search_term, count, repin_count)
    for pin in pins:
        ALL_PINS.append(np.array(pin))


def schedule_activity(amount, target_map, time_to_schedule, time_to_schedule_full, method, arguments, days_amount):
    # checking how many pins we can divide for each day
    items_for_one_day = amount // days_amount
    items_to_add_for_first_day = 0
    if items_for_one_day * days_amount == amount:
        print('Found ', amount, ' items to schedule. Each day ', items_for_one_day, ' will be posted')
    else:
        items_to_add_for_first_day = amount - items_for_one_day * days_amount
        print('Found ', amount, ' items to schedule. Each day ', items_for_one_day,
              ' will be posted. For the first day there will be ', items_to_add_for_first_day, ' added')
    define_intervals(items_for_one_day, time_to_schedule, time_to_schedule_full, target_map)

    day = 0
    item_start_id = 0
    now = datetime.now()

    while day < days_amount:
        start_date_time = now + timedelta(days=day)
        if day > 0:
            schedule_for_single_day(start_date_time, items_for_one_day, item_start_id, time_to_schedule_full,
                                    target_map,
                                    arguments, method)
        elif day == 0:
            schedule_for_today(start_date_time, items_for_one_day, item_start_id, time_to_schedule_full, target_map,
                               arguments, method)

        day += 1
        item_start_id += items_for_one_day
    # scheduling rest of the pins for the first day
    if items_to_add_for_first_day > 0:
        # redefine intervals
        print("Redefining intervals for the rest of the items which will be posted for the first day")
        define_intervals(items_to_add_for_first_day, time_to_schedule, time_to_schedule_full, target_map)
        schedule_for_today(now, items_to_add_for_first_day, item_start_id, time_to_schedule_full, target_map, arguments,
                           method)

    SCHEDULE.start()


def schedule_for_today(start_date_time, items_for_one_day, item_start_id, time_to_schedule_full,
                       target_map, arguments, method):
    current_start_id = item_start_id
    for el in time_to_schedule_full:
        print(time_to_schedule_full[el], " ", target_map[el])

        i = 0

        while i < target_map[el]:
            # if (i+itemStartId in arguments):
            print('Scheduling item ', i + current_start_id)
            # define a date to be scheduled for the item
            now = start_date_time

            time_range_time_start = now.replace(hour=time_to_schedule_full[el][0], minute=0, second=0)
            time_range_time_end = now.replace(hour=time_to_schedule_full[el][1], minute=0, second=0)
            print(now, ' ', time_range_time_start, ' ', time_range_time_end)
            print('CURRENT ARGUMENT', arguments[i + current_start_id])
            if now < time_range_time_start:
                print('now is earlier than ', time_range_time_start,
                      ", scheduling the activity in the random time between ", time_to_schedule_full[el])
                run_date = now.replace(hour=time_to_schedule_full[el][0], minute=randint(0, 59), second=0)
                print(run_date)
                SCHEDULE.add_job(method, 'date', run_date=run_date,
                                 args=arguments[i + current_start_id])

            elif time_range_time_start <= now < time_range_time_end:
                minute = now.minute
                # if this is the end of the time range, we SCHEDULE for an end hour of the time range, plus one minute
                if minute >= 58:
                    run_date = now.replace(hour=time_to_schedule_full[el][1], minute=1, second=0)
                else:
                    run_date = now.replace(hour=time_to_schedule_full[el][0], minute=randint(minute + 1, 59), second=0)
                    print('now is beween the', time_to_schedule_full[el], ', scheduling the activity at ', run_date)
                SCHEDULE.add_job(method, 'date', run_date=run_date,
                                 args=arguments[i + current_start_id])
            elif now >= time_range_time_end:
                run_date = now.replace(hour=time_to_schedule_full[el][0], minute=randint(0, 59), second=0) + timedelta(
                    days=1)
                print('now is later than ', time_range_time_end,
                      ", scheduling the activity for the next day same time: ",
                      run_date)
                SCHEDULE.add_job(method, 'date', run_date=run_date,
                                 args=arguments[i + current_start_id])
            i += 1
        current_start_id += target_map[el]


# Logic for scheduling for any day other than today
def schedule_for_single_day(start_date_time, items_for_one_day, item_start_id, time_to_schedule_full, target_map,
                            arguments, method):
    current_start_id = item_start_id
    for el in time_to_schedule_full:
        print(time_to_schedule_full[el], " ", target_map[el])

        i = 0

        while i < target_map[el]:
            # if (i+itemStartId in arguments):
            print('Scheduling item ', i + current_start_id);
            # define a date to be scheduled for the item
            now = start_date_time
            time_range_time_start = now.replace(hour=time_to_schedule_full[el][0], minute=0, second=0)
            time_range_time_end = now.replace(hour=time_to_schedule_full[el][1], minute=0, second=0)
            print(now, ' ', time_range_time_start, ' ', time_range_time_end)
            print('CURRENT ARGUMENT', arguments[i + current_start_id])

            print("scheduling the activity in the random time between ", time_to_schedule_full[el])
            run_date = now.replace(hour=time_to_schedule_full[el][0], minute=randint(0, 59), second=0)
            print(run_date)
            SCHEDULE.add_job(method, 'date', run_date=run_date,
                             args=arguments[i + current_start_id])

            i += 1
        current_start_id += target_map[el]


def define_intervals(count, time_to_schedule, time_to_schedule_full, target_map):
    time_ranges_count = 0
    time_items_count = 0
    time_ranges_full_count = 0

    print('time_to_schedule', time_to_schedule)
    for time_interval in time_to_schedule:
        previous_hour = None
        time_items_count += 1

        # init items map
        print('time_interval', time_interval)
        for hour in time_interval:

            if previous_hour is not None:
                # time range is time between two hours
                time_ranges_count += hour - previous_hour
                # populate actual hour-by-hour time ranges
                i = 1
                previous_hour_store = previous_hour
                while i <= hour - previous_hour:
                    time_to_schedule_full[time_ranges_full_count] = [previous_hour_store, previous_hour_store + 1]
                    time_ranges_full_count += 1
                    previous_hour_store += 1
                    i += 1

            previous_hour = hour

    print("TIME_RANGES_COUNT", time_ranges_count)
    avg_items_per_time_range = int(round(count / time_ranges_count))
    print("items_per_time_range", avg_items_per_time_range)
    items_assigned_to_intervals = avg_items_per_time_range * time_ranges_count
    print(items_assigned_to_intervals)
    print('time_to_schedule_full', time_to_schedule_full)

    # if we have too many items after rounding
    if items_assigned_to_intervals > count:
        avg_items_per_time_range = avg_items_per_time_range - 1
        items_assigned_to_intervals = avg_items_per_time_range * time_ranges_count
        print('items_assigned_to_intervals', items_assigned_to_intervals)
    # populate intervals pith amounts of items
    i = 0
    while i < time_ranges_count:
        target_map[i] = avg_items_per_time_range
        i += 1

    if items_assigned_to_intervals != count:
        items_to_add_amount = count - items_assigned_to_intervals
        # check_all_items = 0
        i = 0
        while i < items_to_add_amount:
            map_id = randint(0, time_ranges_count - 1)
            target_map[map_id] += 1
            i = i + 1
    print('target_map: ', target_map)


def difference(list1, list2):
    list_dif = [i for i in list1 + list2 if i not in list1 or i not in list2]
    print('LIST DIFF', len(list_dif))
    return list_dif


"""def time_ranges_reader():
    with open(TIME_RANGES_FILENAME, 'r') as file:
        reader = csv.reader(file)
        rowcount = 0
        for row in reader:
            if rowcount != 0:
                print(row)
                TIME_RANGES.append([int(row[0]), int(row[1])])
            rowcount += 1"""


def upload_pins():
    # get current pin from folder
    pin_data = read_dir()
    print(pin_data)
    # Check whether the pins is on the publication today
    if pin_data is not None:

        pin_responce = pinterest.upload_pin(
            section_id=None,
            image_file=pin_data.get("imagePath"),
            link=pin_data.get("jsonData").get("link"),
            title=pin_data.get("jsonData").get("title"),
            board_id=pin_data.get("jsonData").get("board_id"),
            description=pin_data.get("jsonData").get("description"),
        )
        responce_data = json.loads(pin_responce.content)
        pin_id = responce_data["resource_response"]["data"]["id"]
        # Create new pin in own Database
        Pin(search_term='my_pins', board_id=pin_data.get("jsonData").get("board_id"),
            pin_id=pin_id, type_pin='ownPin', is_unique=1, link=pin_data.get("jsonData").get("link"),
            title=pin_data.get("jsonData").get("title"), description=pin_data.get("jsonData").get("description"),
            status=1).insert()
        # copy dir and remove after create in Pinterest and Database
        copy_dir(pin_data.get("folderName"))
        # # Start rePin
        for board in Board.query.filter(is_collaboration=1).all():
            request = pinterest.repin(board_id=board.board_id, pin_id=pin_id)
            if request.status_code == 200:
                Pin(pin_id=pin_id).update(dict(status=1))
            else:
                pass

        return responce_data
    else:
        return f"Today | {str(datetime.now().date())} | you haven't own pins."


def set_boards():
    # Get all own boards
    boards = pinterest.boards(username=pinterest.username, page_size=100)
    for board in boards:
        Board(name=board.get('name'), board_id=board.get('id'),
              is_collaboration=board.get('is_collaborative') if 1 else 0).insert()
    return get_boards()


def get_boards():
    result = []
    for board in Board.query.all():
        result.append({"board_id": board.board_id, "name": board.name,
                       "is_collaborative": board.is_collaboration})
    return result


def read_dir():
    path = Path().absolute().joinpath(Config.UPLOAD_MY_PINS_FOLDER)
    # result = []
    for entry in os.listdir(path):
        if not entry == '.DS_Store':
            if os.path.isdir(os.path.join(path, entry)):
                print(f"Folder: {entry}")
                for pin in os.listdir(path.joinpath(entry)):
                    image_path = ""
                    json_data = ""
                    if pin.endswith('.json'):
                        json_data = json.loads(open(path.joinpath(entry).joinpath(pin), "r").read())
                    else:
                        image_path = str(path.joinpath(entry).joinpath(pin))

                    return {'imagePath': image_path, 'jsonData': json_data, 'folderName': entry}
        #     else:
        #         pass
        # else:
        #     pass
    # return result


def copy_dir(folderName, symlinks=False, ignore=None):
    from_path = Path().absolute().joinpath(f"uploads/pins/{folderName}")
    to_path = Path().absolute().joinpath(f"uploads/all pins/{folderName}")

    if not os.path.exists(to_path):
        os.makedirs(to_path)
    for item in os.listdir(from_path):
        s = os.path.join(from_path, item)
        d = os.path.join(to_path, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, symlinks, ignore)
        else:
            if not os.path.exists(d) or os.stat(s).st_mtime - os.stat(d).st_mtime > 1:
                shutil.copy2(s, d)

    delete_dir(from_path)
    print(f"Folder | {folderName} | was be copied!")
    return f"Folder | {folderName} | was be copied!"


def delete_dir(folderName):
    path = Path().absolute().joinpath(f"uploads/pins/{folderName}")
    if os.path.isdir(path):
        shutil.rmtree(path)
        print(f"Folder | {folderName} | was be deleted!")
    else:
        print(f"Folder | {folderName} | not exist and has not been deleted!")
