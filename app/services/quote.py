import csv
import json
import os
import uuid
import textwrap
from pathlib import Path
from random import randint
from PIL import Image, ImageDraw, ImageFont

from app.models import Quote
from app.services import utils
from config import Config, QuotePinterestConfig


def define_text_height(paragraph, draw, font):
    """
    Define Text Height for input paragraph
    :param font:
    :param draw:
    :param paragraph:
    :return:
    """
    # Init variable textHeight, default = 0
    text_height = 0

    for element in paragraph:
        weight, height = draw.textsize(element, font=font)
        text_height += height

    return text_height


def define_rgb():
    """
    Generated RGB colors for background image
    :return:
    """
    return randint(224, 255), randint(219, 255), randint(207, 255)


def fonts(font, font_size):
    """
    Get Image Font
    :param font:
    :param font_size:
    :return:
    """
    return ImageFont.truetype(font, font_size)


def check_size(description, draw, font, fontSize, fontWidth):
    paragraph = textwrap.wrap(description, width=fontWidth + 2)
    desc_height = define_text_height(paragraph, draw, fonts(font, fontSize))
    if desc_height < 440:
        return desc_height, fontSize, fontWidth
    else:
        return check_size(description, draw, font, fontSize - 1, fontWidth + 3)


def draw_image(rgb):
    """
    Implementation of image creation and draw image.
    :return:
    """
    image = Image.new('RGB', (QuotePinterestConfig.WIDTH, QuotePinterestConfig.HEIGHT), rgb)
    draw = ImageDraw.Draw(image)
    return image, draw


def draw_logo(image, rgb, logo):
    """
    Drawing Logo for image
    :param logo:
    :param image:
    :param rgb:
    :return:
    """
    # Check whether file exists
    if os.path.exists(logo):
        # Getting Image object for LOGO
        logo = Image.open(logo)
        # Take a thumbnail
        logo.thumbnail((100, 100))
        # Getting image size width and height
        img_w, img_h = logo.size
        # Take a background font
        main = Image.new('RGB', (QuotePinterestConfig.WIDTH, 150), rgb)
        # Getting image size width and height
        bg_w, bg_h = main.size
        # Set a offset width and height
        offset = ((bg_w - img_w) // 2, (bg_h - img_h) // 2)
        # Merging two images
        main.paste(im=logo, box=offset, mask=logo)
        # Merging two images
        image.paste(main, (0, QuotePinterestConfig.HEIGHT - 150))
        return image, img_h * 2
    else:
        return image, 150


def draw_author(draw, desc, desc_height, font_size, font, color_text):
    """
    Draw Author
    :param color_text:
    :param font:
    :param font_size:
    :param draw:
    :param desc:
    :param desc_height:
    :return:
    """
    height, padding = (QuotePinterestConfig.HEIGHT + desc_height) / 2, 20
    for line in textwrap.wrap(desc, width=25):
        # Getting image size width and height
        w, h = draw.textsize(line, font=fonts(font, font_size))
        draw.text(
            xy=(QuotePinterestConfig.WIDTH / 2, height), text=desc, font=fonts(font, font_size - 10), fill=color_text)
        height += h + padding
    return draw


def draw_description(draw, desc, desc_height, logo_height, font_size, font_width, font, color_text):
    """
    Draw Description
    :param color_text:
    :param font:
    :param font_width:
    :param font_size:
    :param logo_height:
    :param draw:
    :param desc:
    :param desc_height:
    """
    paragraph = textwrap.wrap(desc, width=font_width)
    current_h, padding = ((QuotePinterestConfig.HEIGHT + logo_height) / 2) - desc_height, 5
    for line in paragraph:
        w, h = draw.textsize(line, font=fonts(font, font_size))
        draw.text(((QuotePinterestConfig.WIDTH - w) / 2, current_h), line, font=fonts(font, font_size), fill=color_text)
        current_h += h + padding


def quotes(desc, bc_rgb, logo, font, color_text, author, path):
    """
    Generate quotes
    :param author:
    :param desc:
    :param color_text:
    :param font:
    :param logo:
    :param bc_rgb:
    """
    # Get image and draw image.
    image, draw = draw_image(rgb=bc_rgb)
    logo_height = 0
    if logo:
        image, logo_height = draw_logo(image, bc_rgb, logo)
    # Define description Height
    desc_height, font_size, font_width = check_size(
        desc, draw, font, QuotePinterestConfig.FONT_SIZE, QuotePinterestConfig.FONT_WIDTH)
    draw_description(draw, desc, desc_height, logo_height, font_size, font_width, font, color_text)
    if author:
        draw_author(draw, author, desc_height, font_size, font, color_text)

    file_name = str(uuid.uuid4())
    dir_name = utils.create_dir(Config.UPLOAD_QUOTES_FOLDER + file_name)
    if path == '':
        image_path = f'uploads/quotes/{dir_name}/{file_name}.png'
    else:
        image_path = path
    image_path = f'uploads/quotes/{dir_name}/{file_name}.png'
    image.save(image_path)

    return image_path, file_name


text = 'The rain in Spain falls mainly on the plains,  mainly on the plains.' \
       'The rain in Spain falls mainly on the plains,  mainly on the plains.' \
       'The rain in Spain falls mainly on the plains,  mainly on the plains.' \
       'The rain in Spain falls mainly on the plains,  mainly on the plains.'


def draw_quotes(file_path):
    reader = csv.DictReader(open(file_path, 'r'))
    for row in reader:
        keys = row.keys()
        csv_data = {}
        for key in keys:
            csv_data.update({key.replace(' ', '_').lower(): row.get(key)})

        link = row.get('Link')
        logo = row.get('Logo')
        font = row.get('Font')
        title = row.get('Title')
        quote = row.get('Quotes')
        author = row.get('Author')
        desc = row.get('Description')
        board_id = row.get('Board ID')
        tc_rgb = row.get('Text Color')
        search_term = row.get('Search Term')
        bc_rgb = row.get('Background Color')
        if quote != '':
            image_path, file_name = quotes(desc=quote, bc_rgb=is_background_color(bc_rgb), logo=is_logo(logo),
                                           font=is_font(font), color_text=is_text_color(tc_rgb),
                                           author=is_author(author), path='')
            # Create metadata file
            metadata_path = create_metadata(csv_data, file_name)

            Quote(quotes=quote, title=title, description=desc, author=author, link=link, logo=logo,
                  search_term=search_term, board_id=board_id, background_color=bc_rgb, text_color=tc_rgb,
                  image_path=image_path, data_path=metadata_path).insert()
    # Deleting upload file after logic.
    utils.remove_files(file_path)


def is_logo(status):
    if status == "TRUE":
        path = Config.DEFAULT_LOGO_PATH
        if os.path.isfile(path):
            return path
        else:
            return False

    elif status == "FALSE":
        return False


def is_author(value):
    if value != '':
        return value
    else:
        return False


def is_text_color(value):
    if value != '':
        return f'rgb({value})'
    else:
        return Config.DEFAULT_QUOTE_COLOR_TEXT


def is_background_color(value):
    if value != '':
        return f'rgb({value})'
    else:
        return define_rgb()


def is_font(value):
    if value != '':
        path = Config.UPLOAD_FONTS_FOLDER + str(f'/{value}.ttf')
        if os.path.isfile(path):
            return path
        else:
            return Config.DEFAULT_FONT_PATH
    else:
        return Config.DEFAULT_FONT_PATH


def read_fonts():
    path = Path().absolute().joinpath('uploads/fonts')
    result = []
    for entry in os.listdir(path):
        result.append(entry)

    return result


def create_metadata(metadata, folder_name):
    path = Config.UPLOAD_QUOTES_FOLDER + folder_name + '/metadata.json'
    open(path, 'w').write(json.dumps(metadata))
    return path
