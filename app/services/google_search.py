from pathlib import Path

from app.models import Pin
from app.services import database
from google_images_search import GoogleImagesSearch
from config import Config


# === START CONSTANTS ===
PATH_TO_GOOGLE_IMAGES = Path().absolute().joinpath(Config.GOOGLE_IMAGES_PINS_FOLDER)
# === END CONSTANTS ===

# if you don't enter api key and cx, the package will try to search
# them from environment variables GCS_DEVELOPER_KEY and GCS_CX
gis = GoogleImagesSearch('AIzaSyBKSKXXZQLDAz59C2acUcCTZizvWshot6A', '008484789061085334797:55m1pyt5pwo')

# define search params:
_search_params = {
    'q': 'design',
    'num': '10',
    'start': '1',
    # 'safe': 'medium',
    'fileType': 'jpeg,gif,png,jpg,svg',
    # 'imgType': 'photo',
    # 'imgSize': "large",
    # 'imgDominantColor': 'black|blue|brown|gray|green|pink|purple|teal|white|yellow'
}

# this will only search for images:
# gis.search(search_params=_search_params)

# this will search and download:
# gis.search(search_params=_search_params, path_to_dir='../assets/google_images')
# print(gis.results())

# this will search, download and resize:
# gis.search(search_params=_search_params, path_to_dir='/path/', width=500, height=500)


# search first, then download and resize afterwards
def search_results(search_term, board_id, totalCount):
    _search_params["q"] = search_term
    print(_search_params)
    results = []
    gis.search(search_params=_search_params)
    batch = gis.results()
    while len(batch) > 0 and len(results) < totalCount:
        for image in batch:
            print(image.title)
            Pin(pin_id=image.url, search_term=search_term, board_id=board_id,
                type_pin='google', link=image.url, title=image.title, description=image.snippet).insert()
            results.append(image.url)

        _search_params['start'] = str(int(_search_params['num']) + int(_search_params['start']))
        gis.search(search_params=_search_params)
        batch = gis.results()
        print(results)

    #return database.count_pins_by_type(board_id, "google", search_term)
    #
    # for image in results:
    #     print(image.title)
    #     database.db_insert_google_pin(conn, {
    #         "pin_id": image.url,
    #         "search_term": search_term,
    #         "board_id": board_id,
    #         "type_pin": "google",
    #         "link": image.url,
    #         "title": str(image.title),
    #         "description": str(image.snippet),
    #     })
    # print(image.title)
    # print(image.snippet)
    # image.download("assets/google_images")

    while len(os.listdir(PATH_TO_GOOGLE_IMAGES)) <= totalCount:
        _search_params['start'] = str(int(_search_params['num']) + int(_search_params['start']))
        gis.search(search_params=_search_params)
        for image in gis.results():
            database.db_insert_google_pin(conn, {
                "pin_id": image.url,
                "search_term": search_term,
                "board_id": board_id,
                "type_pin": "google",
                "link": image.url,
                "title": str(image.title),
                "description": str(image.snippet),
            })

    print("You have the required number of photos from the Google!")


# searchResults("vintage interior design victorian", "600315894019168239", 20)
