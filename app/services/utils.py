import os
import shutil
from zipfile import ZipFile
from config import Config


def zip_extract(file_path, file_name):
    # opening the zip file in READ mode
    path = Config.UPLOAD_MY_PINS_FOLDER + os.path.splitext(file_name)[0]
    if os.path.exists(path):
        remove_dir(path)

    with ZipFile(file_path, 'r') as zip_file:
        for name in zip_file.namelist():
            if name.endswith('.DS_Store'):
                continue
            elif name.startswith('_'):
                continue
            zip_file.extract(name, Config.UPLOAD_MY_PINS_FOLDER)

    remove_files(file_path)


def create_dir(path):
    if os.path.exists(path):
        try:
            os.mkdir(path)
            print(f'Directory | {path} | has been created!')
            return path
        except OSError:
            return print("Error while creating directory ", path)


def mkdir(path):
    if not os.path.exists(path):
        try:
            os.mkdir(path)
            print(f'Directory | {path} | has been created!')
            return path
        except OSError as e:
            print(e)
            return print("Error while creating directory ", path)


def remove_dir(path):
    if os.path.exists(path):
        try:
            shutil.rmtree(path)
            return print(f'Directory | {path} | has been deleted!')
        except OSError:
            return print("Error while deleting directory ", path)


def remove_files(file_path):
    if os.path.exists(file_path):
        try:
            os.remove(file_path)
            return print(f'File | {file_path} | has been deleted!')
        except OSError:
            return print("Error while deleting file ", file_path)


def create_upload_folder_structure(list_folders):
    for folder_path in list_folders:
        create_dir(folder_path)

    return print(f'Default folder structure has been created | {list_folders} |.')
