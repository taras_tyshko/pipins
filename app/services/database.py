import os

from sqlalchemy import func

from app import db, app, pin
from app.models import Pin, Board, Follow, Quote
from flask_sqlalchemy import SQLAlchemy

from config import Config, basedir

ADDITIONAL_SEARCH_COUNTER = 0
MAX_SEARCH_ITERATIONS = 1
LAST_SEARCH_TERM = ''


def get_search_terms():
    pins = Pin.query.group_by(Pin.search_term).filter_by(status=0).with_entities(Pin.search_term).all()
    print(pins)
    result = []
    for pin in pins:
        result.append(pin.search_term)

    return result


def get_pins_by_terms(search_term, amount, repin_count):
    global ADDITIONAL_SEARCH_COUNTER
    global MAX_SEARCH_ITERATIONS
    global LAST_SEARCH_TERM

    if LAST_SEARCH_TERM != search_term and LAST_SEARCH_TERM != '':
        ADDITIONAL_SEARCH_COUNTER = 0

    # Getting all pins from database

    rows = Pin.query.filter_by(status=0, search_term=search_term).with_entities(
        Pin.pin_id, Pin.board_id).limit(amount).all()
    print(rows)


    if amount > len(rows) > 0:
        pins_needed = amount - len(rows)
        print("There are not enough pins for a search term '", search_term, "'. ", pins_needed, " more are needed.")
        if ADDITIONAL_SEARCH_COUNTER < MAX_SEARCH_ITERATIONS:
            try:
                # defining board id - to change later to be done once only
                current_pin = Pin.query.filter_by(search_term=search_term).first()
                pin.search(search_term, repin_count, current_pin.board_id)
                ADDITIONAL_SEARCH_COUNTER += 1
                get_pins_by_terms(conn, search_term, amount, repin_count)
            except:
                print('There was an error while doing the additional search by a search term ', search_term)
                ADDITIONAL_SEARCH_COUNTER += 1

    LAST_SEARCH_TERM = search_term
    return rows


def check_if_follow(user_id):
    items = Follow.query.filter_by(user_id=user_id, we_follow=1).all()
    if len(items) == 0:
        return False
    else:
        return True


def counts_pins():
    result = []
    count = 0
    rows = Pin.query.group_by(Pin.search_term).filter_by(status=0).with_entities(
        Pin.search_term, Pin.board_id, func.count(Pin.ID)).all()
    for row in rows:
        data = Pin.query.group_by(Pin.search_term).filter_by(status=1, board_id=row[1],
                                                             search_term=row[0]).with_entities(func.count(Pin.ID)).all()
        if data:
            count = data[0][0]

        result.append({'Search Term': row[0], 'Number of Unpublished pins': row[2],
                       'Board ID': row[1], 'Number of Published Pins': count})
    return result


def count_pins_by_type(board_id, type_pin, search_term):
    count = Pin.query.filter_by(type_pin=type_pin, status=0, board_id=board_id,
                                search_term=search_term).with_entities(func.count(Pin.pin_id)).all()
    return count[0][0]
