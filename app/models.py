from datetime import datetime

from sqlalchemy import exc

from app import db


class TimestampMixin(object):
    created_at = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, onupdate=datetime.utcnow)


class Quote(TimestampMixin, db.Model):
    __tablename__ = 'quotes'
    ID = db.Column(db.Integer, primary_key=True, autoincrement=True)
    quotes = db.Column(db.Text(), index=True, unique=True, nullable=False)
    title = db.Column(db.Text(), default=quotes)
    description = db.Column(db.Text(), default=title)
    author = db.Column(db.Text())
    link = db.Column(db.Text())
    background_color = db.Column(db.Text())
    text_color = db.Column(db.Text())
    logo = db.Column(db.Integer(), default=0)
    image_path = db.Column(db.Text())
    data_path = db.Column(db.Text())
    search_term = db.Column(db.Text(), nullable=False)
    board_id = db.Column(db.Text(), nullable=False)

    def __init__(self, **kwargs):
        super(Quote, self).__init__(**kwargs)

    def insert(self):
        try:
            db.session.add(self)
            db.session.commit()
            return f"Quote | ID: {self.ID} | has been created!"
        except exc.IntegrityError:
            pass
        except exc.InvalidRequestError:
            pass

    def update(self, kwargs):
        Quote.query.filter_by(ID=self.ID).update(kwargs)
        db.session.commit()
        return f"Quote | ID: {self.ID} | has been updated!"

    def delete(self):
        item = Quote.query.get(self.ID)
        db.session.delete(item)
        db.session.commit()
        return f"Quote | ID: {self.ID} | has been deleted!"


class Pin(TimestampMixin, db.Model):
    __tablename__ = 'pins'
    ID = db.Column(db.Integer, primary_key=True, autoincrement=True)
    pin_id = db.Column(db.Integer(), index=True, unique=True, nullable=False)
    search_term = db.Column(db.Text(), index=True)
    board_id = db.Column(db.Text(), index=True)
    status = db.Column(db.Integer(), default=0)
    type_pin = db.Column(db.Text(), default='rePin')
    is_unique = db.Column(db.Integer(), default=0)
    is_collaboration = db.Column(db.Integer(), default=0)
    link = db.Column(db.Text())
    title = db.Column(db.Text())
    description = db.Column(db.Text())

    def __init__(self, **kwargs):
        super(Pin, self).__init__(**kwargs)

    def insert(self):
        try:
            db.session.add(self)
            db.session.commit()
            return f"Pin | pin_id: {self.pin_id} | has been created!"
        except exc.IntegrityError:
            pass
        except exc.InvalidRequestError:
            pass

    def update(self, kwargs):
        Pin.query.filter_by(pin_id=self.pin_id).update(kwargs)
        db.session.commit()
        return f"Pin | ID: {self.ID} | has been updated!"

    def delete(self):
        item = Pin.query.get(self.ID)
        db.session.delete(item)
        db.session.commit()
        return f"Pin | pin_id: {item.pin_id} | has been deleted!"

    def get_pin_ids(self):
        return Pin.query.filter_by(
            search_term=self.search_term, board_id=self.board_id).all()


class Board(TimestampMixin, db.Model):
    __tablename__ = 'boards'
    ID = db.Column(db.Integer, primary_key=True, autoincrement=True)
    board_id = db.Column(db.Text(), index=True, unique=True)
    name = db.Column(db.Text())
    is_collaboration = db.Column(db.Integer(), default=0)

    def __init__(self, **kwargs):
        super(Board, self).__init__(**kwargs)

    def insert(self):
        db.session.add(self)
        db.session.commit()
        return print(f"Board | board_id: {self.board_id} | has been created!")

    def update(self, kwargs):
        Board.query.filter_by(ID=self.ID).update(kwargs)
        db.session.commit()
        return f"Board | ID: {self.ID} | has been updated!"

    def delete(self):
        item = Board.query.get(self.ID)
        db.session.delete(item)
        db.session.commit()
        return f"Board | ID: {item.ID} | has been deleted!"


class Follow(TimestampMixin, db.Model):
    __tablename__ = 'follows'
    ID = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Text, index=True)
    we_follow = db.Column(db.Integer, default=0)
    they_follow = db.Column(db.Integer, default=0)

    def __init__(self, **kwargs):
        super(Follow, self).__init__(**kwargs)

    def insert(self):
        db.session.add(self)
        db.session.commit()
        return f"Follow | user_id: {self.user_id} | has been created!"

    def update(self, kwargs):
        Follow.query.filter_by(user_id=self.user_id).update(kwargs)
        db.session.commit()
        return f"Follow | user_id: {self.ID} | has been updated!"

    def delete(self):
        item = Follow.query.get(self.ID)
        db.session.delete(item)
        db.session.commit()
        return f"Follow | user_id: {item.user_id} | has been deleted!"
