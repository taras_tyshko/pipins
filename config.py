import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.urandom(42) or 'Pinterest-Service'
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    UPLOAD_FOLDER = 'uploads/files/'
    UPLOAD_TMP_FOLDER = 'uploads/tmp/'
    UPLOAD_PINS_FOLDER = 'uploads/pins/'
    UPLOAD_LOGO_FOLDER = 'uploads/logo/'
    UPLOAD_FONTS_FOLDER = 'uploads/fonts/'
    UPLOAD_IMAGES_FOLDER = 'uploads/images/'
    UPLOAD_QUOTES_FOLDER = 'uploads/quotes/'
    UPLOAD_MY_PINS_FOLDER = 'uploads/my_pins/'

    # Default folder list
    FOLDER_LIST = [UPLOAD_FOLDER, UPLOAD_TMP_FOLDER, UPLOAD_QUOTES_FOLDER, UPLOAD_FONTS_FOLDER,
                   UPLOAD_LOGO_FOLDER, UPLOAD_PINS_FOLDER, UPLOAD_IMAGES_FOLDER, UPLOAD_MY_PINS_FOLDER]

    ALLOWED_FILE_EXTENSIONS = {'csv', 'png', 'jpeg', 'jpg', 'ttf', 'zip'}
    MAX_CONTENT_LENGTH = 16 * 1024 * 1024
    DEFAULT_FONT_PATH = 'uploads/fonts/Pacifico-Regular.ttf'
    DEFAULT_LOGO_PATH = 'uploads/logo/logo.png'
    DEFAULT_QUOTE_COLOR_TEXT = 'rgb(128,128,128)'
    GOOGLE_ARTICLES_PINS_FOLDER = 'uploads/google_articles/'
    GOOGLE_IMAGES_PINS_FOLDER = 'assets/google_images'
    SCHEDULE_SET_FOLDER = 'uploads/schedule_set/'
    PIN_TITLE_MAX_LENGTH = 100
    PIN_DESCR_MAX_LENGTH = 500


class QuotePinterestConfig(object):
    WIDTH = 600
    HEIGHT = 900
    FONT_SIZE = 40
    FONT_WIDTH = 25


class QuoteInstagramConfig(object):
    WIDTH = 600
    HEIGHT = 900
    FONT_SIZE = 40
    FONT_WIDTH = 25
